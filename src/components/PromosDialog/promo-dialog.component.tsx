import React from 'react'
import { useTheme } from '@material-ui/core'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination } from 'swiper';

import './promo-dialog.scss'
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';

const Slides = [
    { name: '2x1 en Bebidas Nacionales', image: 'https://digital-menu-dev.s3-us-west-2.amazonaws.com/restaurants/Elkar-Restaurante.png' },
    { name: '2x1 en Bebidas Nacionales', image: 'https://digital-menu-dev.s3-us-west-2.amazonaws.com/restaurants/Elkar-Restaurante.png' },
    { name: '2x1 en Bebidas Nacionales', image: 'https://digital-menu-dev.s3-us-west-2.amazonaws.com/restaurants/Elkar-Restaurante.png' },
    { name: '2x1 en Bebidas Nacionales', image: 'https://digital-menu-dev.s3-us-west-2.amazonaws.com/restaurants/Elkar-Restaurante.png' },

]

SwiperCore.use([Navigation, Pagination]);

export const PromoDialog = () => {
    const theme = useTheme()
    return(
        <div className="promo-dialog">
            <Swiper
                navigation
                pagination={{ clickable: true }}
                spaceBetween={5}
                slidesPerView={1}
            >
                {
                    Slides.map((slide: any, index: number) => (
                        <SwiperSlide key={index+'_promo-slide'}>
                            <div style={{
                                backgroundImage: `url(${slide.image})`,
                                backgroundSize: 'contain',
                                backgroundPosition: 'center',
                                backgroundRepeat: 'no-repeat',
                            }} className="promo-dialog__slide">
                                <span style={{
                                    backgroundColor: theme.palette.primary.main,
                                    color: theme.palette.primary.contrastText,
                                    opacity: 0.8
                                }} className="promo-dialog__slide__title">
                                    {slide.name}
                                </span>
                            </div>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    )
}
