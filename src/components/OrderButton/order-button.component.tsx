import React from 'react'
import { Receipt } from '@material-ui/icons'
import { IconButton } from '@material-ui/core'

import './order-button.scss'

export const OrderButton = ({ setisOpen, left, top }: any) => {
	const setting = JSON.parse(sessionStorage.getItem('settings_digitalmenu') || '{}')

	return(
		<div className="order">
			<IconButton style={{ color: setting?.text_color || 'black'}}  onClick={() => setisOpen(true)} className="order__button">
				<Receipt />
			</IconButton>
		</div>
	)
}