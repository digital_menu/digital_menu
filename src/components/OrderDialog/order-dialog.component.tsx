import { useCookies } from 'react-cookie'
import { useParams } from 'react-router-dom';
import React, { useState, useEffect } from 'react'
import { Button, Divider, useTheme } from '@material-ui/core'
import { RestaurantRounded, ImportContacts } from '@material-ui/icons'

import './order-dialog.scss'

export const OrderDialog = () => {
	const { name }:any = useParams()
    const [cookies, setCookie] = useCookies([`order_list_${name}`])
	// const setting = JSON.parse(sessionStorage.getItem('settings_digitalmenu') || '{}')
    const [total, setTotal] = useState(0)
    const theme = useTheme()
    const changeQuantity = (index: number, value: number) =>{
        let newProducts = [...cookies[`order_list_${name}`]]
        if(newProducts[index].quantity !== 0 || value === 1){
            newProducts[index].quantity = newProducts[index].quantity + value
            setCookie(`order_list_${name}`,newProducts, { path: '/', maxAge: 60*60*3 })
        }
    }
    useEffect(() => {
        if(cookies[`order_list_${name}`]) {
            let newTotal = 0
            for(let product of cookies[`order_list_${name}`]){
                newTotal = newTotal + (product.price * product.quantity)
            }
            newTotal.toFixed(2)
            setTotal(newTotal)
        }
    }, [name])
    
    return(
        <div className="order-dialog">
            <div className="order-dialog__title" style={{ backgroundColor: theme.palette.primary.main, color: theme.palette.primary.contrastText }}>
                <span className="order-dialog__title-text">
                    Mi pedido
                </span>
                <span className="order-dialog__title-text__total">
                    Total : {total.toFixed(2)} MXN
                </span>
            </div>
            <Divider />
            <div className="order-dialog__list">
                {
                    cookies[`order_list_${name}`] ? cookies[`order_list_${name}`].map((product: any, index: number) => (
                        <div key={index+'_products'} className="order-dialog__item">
                            <RestaurantRounded />
                            <div className="order-dialog__item-text" >
                                <span className="order-dialog__item-text__title">{product.name}</span>
                                <br/>
                                <div className="order-dialog__item-text__quantity">
                                    Cantidad: 
                                    <button onClick={()=>changeQuantity(index, -1)} style={{ color: theme.palette.primary.contrastText }} className="order-dialog__item-text__quantity-button">
                                        -
                                    </button>
                                    {product.quantity}
                                    <button onClick={()=>changeQuantity(index, +1)} style={{ color: theme.palette.primary.contrastText }} className="order-dialog__item-text__quantity-button">
                                        +
                                    </button>
                                </div>
                            </div>
                            <span className="order-dialog__item-text__price">{product.price} MXN</span>
                        </div>
                    )): null
                }
                <Divider variant="middle" />
            </div>
            <Button color="primary" variant="contained" className="order-dialog__button">
                <ImportContacts fontSize="large" />
                {'  '}Guardar Pedido
            </Button>
        </div>
    )
}