import React from 'react'
import { IconButton } from '@material-ui/core'
import { Swiper, SwiperSlide } from 'swiper/react'
import { useHistory, useLocation } from 'react-router-dom'
import SwiperCore, { Navigation, Pagination } from 'swiper';
import { ArrowForwardIos } from '@material-ui/icons'

import './carrousel.scss'
import 'swiper/components/navigation/navigation.scss';

SwiperCore.use([Navigation, Pagination]);

export const Carrousel = ({ name, size, title, sliders }: any) => {
	const setting = JSON.parse(sessionStorage.getItem('settings_digitalmenu') || '{}')
	const location = useLocation();
	const history = useHistory();
	const classStyle = () => {
		switch (size) {
			case 'medium':
				return 'carrousel__medium'
			case 'large':
				return 'carrousel__large'
			default:
				return 'carrousel__small'
		}
	}
	const titleStyle = () => {
		switch (size) {
			case 'medium':
				return 'carrousel__slide-title carrousel__slide-title_small'
			case 'large':
				return 'carrousel__slide-title'
			default:
				return 'carrousel__slide-title_small'
		}
	}
	const goTo = (path:string) => {
		history.push(`${location.pathname}/${path}`)
	}

	return(
		<div className="carrousel">
			<h2 style={{ color: setting?.text_color || 'black'}} className="carrousel__title">
				{title}
			</h2>
			<div className="carrousel__swiper">
				<IconButton color="primary" className={`prevButton-${name} carrousel__button carrousel__button_prev`}>
					<ArrowForwardIos />
				</IconButton>
				<Swiper
					navigation={{
						prevEl: `.prevButton-${name}`,
						nextEl: `.nextButton-${name}`
					}}
					spaceBetween={5}
					className={classStyle()}
					slidesPerView={size === "medium"? 2:1}
				>
					{sliders.map((value: any, index: number)=>(
						<SwiperSlide
							onClick={()=>goTo(value.name)}
							style={{ backgroundColor: setting?.secondary_color || 'transparent', backgroundImage: `url(${value.image})`, backgroundSize: 'cover', backgroundPosition: 'center'}}
							className="carrousel__slide" key={index+'_SwipeMenu'}>
							<h2 style={{ color: setting?.secondary_text_color || 'black'}} className={titleStyle()}>
								{value.name}
							</h2>
						</SwiperSlide>
					))}
				</Swiper>
				<IconButton color="primary" className={`nextButton-${name} carrousel__button carrousel__button_next`}>
					<ArrowForwardIos />
				</IconButton>
			</div>
		</div>
	)
}
