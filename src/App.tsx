import React from 'react';
import { DndProvider } from 'react-dnd'
import { QRNavigation } from './navigation'
import { CookiesProvider } from 'react-cookie';
import { TouchBackend } from 'react-dnd-touch-backend'
import { BrowserRouter as Router } from "react-router-dom";

import 'swiper/swiper.scss';


function App() {
  return (
    <CookiesProvider>
      <DndProvider backend={TouchBackend}>
        <Router>
          <QRNavigation />
        </Router>
      </DndProvider>
    </CookiesProvider>
  );
}

export default App;
