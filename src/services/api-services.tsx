import axios from 'axios'

const menuAPI = axios.create({
  baseURL: 'http://192.168.1.85:4000/api'
});

export const getRestaurant = async (restaurant: string) =>
  await menuAPI.get(`/digital-menu/${restaurant}`)

export const getMenu = async (restaurant: string, menu: string) =>
  await menuAPI.get(`/digital-menu/${restaurant}/${menu}`)
