import { Switch, Route } from "react-router-dom";
import { Restaurant, Menu, Landing } from '../../views'
import React from 'react'

export const QRNavigation = () => {
	return(
		<div>
			<Switch>
				<Route path="/digital-menu/:name/:menu">
					<Menu />
				</Route>
				<Route path="/digital-menu/:name">
					<Restaurant />
				</Route>
				<Route path="/">
					<Landing />
				</Route>
			</Switch>
		</div>
	)
}