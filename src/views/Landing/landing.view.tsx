import React from 'react'

import './landing.scss'
import 'swiper/swiper.scss'
import 'swiper/components/pagination/pagination.scss'

import { IconButton } from '@material-ui/core'
import { Swiper, SwiperSlide } from 'swiper/react'
import MenuImages from './../../assets/images/MenuImages.png'
import { ArrowRight, ArrowLeft } from '@material-ui/icons'

const Benefits = [
	{keyword: 'Sin contacto', text: 'Un menú digital sin necesidad de contacto hacia tus comensales.'},
	{keyword: 'Un celular, un menú', text: 'Convierte cada celular en un menú personal'},

]
export const Landing = () => {
	return(
		<div className="landing">
			<div className="landing__content-container">
				<div className="landing__top-block">
					<div className="landing__top-block-text">
						<h1>Menu Maker</h1>
						<p>
							Crea, diseña y personaliza el menu para tu restaurante totalmente digital, sin necesidad de contacto y para todo tipo de telefonos.
						</p>
					</div>
					<div className="landing__top-block-image">
						<img src={MenuImages} alt=""/>
					</div>
				</div>
				<div className="landing__slider-block">
					<div className="landing__slider-block-inner">
						<h2 className="landing__slider-block-title">Beneficios</h2>
						<div className="landing__slider">
						<IconButton className="landing__slider-button landing__slider-button_left">
							<ArrowLeft />
						</IconButton>
						<Swiper
							spaceBetween={37}
							slidesPerView={3}
							pagination={{ 
								clickable: true,
								bulletClass: 'landing__slider-bullet',
								bulletActiveClass: 'landing__slider-active-bullet'
							}}
							onSlideChange={() => console.log('slide change')}
							onSwiper={(swiper) => console.log(swiper)}
						>
							{Benefits.map((benefit:any, index:number)=>(
								<SwiperSlide key={`landing_slide_${index}`}>
									<div className="landing__slide">
										<div className="landing__slide-image"></div>
										<div className="landing__slide-text">
											<b>{benefit.keyword}</b><br/>{benefit.text}
										</div>
									</div>
								</SwiperSlide>
							))}
						</Swiper>
						<IconButton className="landing__slider-button landing__slider-button_right">
							<ArrowRight />
						</IconButton>
					</div>
					</div>
				</div>
				<div className="landing__contact-block">
					<h2>
						¿ Tienes un restaurante y te interesa Menu Maker ?
					</h2>
					<button className="landing__contact-block-button">Contáctanos</button>
				</div>
				<div className="landing__bottom">
					Powered by AcumaDev
				</div>
			</div>
		</div>
	)
}
