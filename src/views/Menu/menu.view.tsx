import { getMenu } from '../../services'
import { useCookies } from 'react-cookie'
import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { OrderButton, OrderDialog } from '../../components'
import { ArrowLeft, Restaurant, Add } from '@material-ui/icons'
import { ThemeProvider, createMuiTheme } from '@material-ui/core'
import { Tabs, Tab, AppBar, Toolbar, Dialog, IconButton } from '@material-ui/core'

import './menu.scss'

type MenuType = {
	name: string
	image: string
	type: string
	sections: Array<Object>
}

const MenuState: MenuType = {
	sections: [],
	name: '',
	image: '',
	type: '',
}

const defaultTheme = createMuiTheme({
	palette: {
	  primary: {
		main: '#fff',
	  },
	  secondary: {
		main: '#fff'
	  }
	}
});

export const Menu = () => {
	const { name, menu }:any = useParams()
	const [cookies, setCookie] = useCookies([`order_list_${name}`,'setting']);
	const [isLoading, setisLoading] = useState(false)
	const [theme, setTheme] = useState(defaultTheme)
	const [Menu, setMenu] = useState(MenuState)
	const [isOpen, setisOpen] = useState(false)
	const [TabIndex, setTabIndex] = useState(0)
	const history = useHistory()

	useEffect(() => {
		setisLoading(true)
		if(sessionStorage.getItem('settings_digitalmenu')){
			const setting = JSON.parse(sessionStorage.getItem('settings_digitalmenu') || '{}')
			setTheme(createMuiTheme({
				palette: {
				  primary: {
					main: setting.main_color,
					contrastText: setting.text_color,
				  },
				  secondary: {
					main: setting.secondary_color,
					contrastText: setting.secondary_text_color,
				  }
				}
			}))
		}
		getMenu(name, menu)
		.then(resp =>{
			setMenu(resp.data)
			setisLoading(false)
		})
		.catch(()=>setisLoading(false))
	}, [name, menu])

	const handleChange = (event: any, newValue: number) => {
		setTabIndex(newValue)
	}
	const addDish2Order = (dish: any) => {
		dish["quantity"] = 1
		if(cookies.order_list){
			setCookie(`order_list_${name}`,[...cookies.order_list, dish], { path: '/', maxAge: 60*60*3 })
		} else {
			setCookie(`order_list_${name}`,[dish], { path: '/', maxAge: 60*60*3 })
		}
	}

	return !isLoading? (
		<ThemeProvider theme={theme}>
			<div  className="menu">
				<AppBar className="header header_no-icon">
					<Toolbar>
						<div onClick={() => history.push(`/digital-menu/${name}`)} className="header__back-icon">
							<ArrowLeft style={{ color: `${theme.palette.primary.contrastText}` }} fontSize="large" />
							<span style={{ color: theme.palette.primary.contrastText }}>Regresar</span>
						</div>
						<div className="header__title">
							{Menu.name.replace('-', ' ') }
						</div>
					</Toolbar>
				</AppBar>
				<div className="menu__main">
					<Tabs
						className="menu__tabs"
						value={TabIndex}
						onChange={handleChange}
						scrollButtons="auto"
						variant="scrollable"
						style={{ color: `${theme.palette.primary.contrastText}` }}
					>
					{
						Menu.sections.map((value: any, index)=>(
							<Tab label={value.name} key={index+'_tab_section'} />
						))
					}
					</Tabs>
					{
						Menu.sections.map((value: any, index) => (
							<div role="tabpanel" hidden={TabIndex !== index} key={index+'_section'}>
								{
									value.back_image?
									<div className="section__image" style={{ backgroundImage: `url('${value.back_image}')` }}>
									</div>:null
								}
								<div style={{ color: theme.palette.primary.contrastText }} className="section__title">
									{value.name}
								</div>
								<div style={{ color: theme.palette.primary.contrastText }} className="section__description">
										{value.description}
								</div>
								{
									value.dishes.length > 0?
									value.dishes.map((dish: any, indexDish: number) =>(
										<div className="menu__dish" key={indexDish+'_dishes'}>
											<div className="dish__title">
												<Restaurant style={{ color: theme.palette.secondary.contrastText }} fontSize="default" />
												<span style={{ color: theme.palette.secondary.contrastText }} >
													{dish.name}
												</span>
												<IconButton onClick={()=>addDish2Order(dish)}>
													<Add style={{ color: theme.palette.secondary.contrastText }} />
												</IconButton>
											</div>
											{
												dish.image?
												<img alt="dish" className="dish__image" src={dish.image}  />
												:null

											}
											<div style={{ color: theme.palette.secondary.contrastText }} className="dish__description">
												{dish.description}
											</div>
											<div style={{ color: theme.palette.secondary.contrastText }} className="dish__price">
												{`${dish.price} MXN`}
											</div>
										</div>
									)) : null
								}
							</div>
						))
					}
				</div>
				<OrderButton top={90} left={56} setisOpen={() => setisOpen(true)} />
				<Dialog
					open={isOpen}
					onClose={() => setisOpen(false)}
				>
					<span onClick={() => setisOpen(false)} className="menu__close">&times;</span>
					<OrderDialog />
				</Dialog>
			</div>
		</ThemeProvider>
	):null
}
