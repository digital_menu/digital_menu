import { AppBar, Toolbar } from '@material-ui/core'
import { useCookies } from 'react-cookie'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Skeleton } from '@material-ui/lab'
import { Dialog } from "@material-ui/core"

import { ThemeProvider, createMuiTheme } from '@material-ui/core'
import { Carrousel, OrderButton, OrderDialog } from '../../components'
import { getRestaurant } from '../../services'

import './restaurant.scss'
import 'swiper/swiper.scss'

type RestaurantType = {
	name: string
	image: string
	address: string
	menus: Array<Object>
}


const RestaurantState: RestaurantType = {
	menus: [],
	name: '',
	image: '',
	address: '',
}

const defaultTheme = createMuiTheme({
	palette: {
	  primary: {
		main: '#fff',
	  },
	  secondary: {
		main: '#fff'
	  }
	}
});

export const Restaurant = () => {
	const [Restaurant, setRestaurant] = useState(RestaurantState)
	const [cookies, setCookie] = useCookies(['show_promos', 'setting'])
	const [isLoading, setisLoading] = useState(false)
	const [theme, setTheme] = useState(defaultTheme)
	const [isPromo, setisPromo] = useState(true)
	const [isOpen, setisOpen] = useState(false)
	let { name }:any = useParams()

	useEffect(() => {
		setisLoading(true)
		if(cookies.show_promos === undefined) {
			setCookie('show_promos', true, { maxAge: 60 })
			setisPromo(true)
		} else {
			setisPromo(JSON.parse(cookies.show_promos.toLowerCase()))
		}
		getRestaurant(name)
		.then(resp =>{
			setRestaurant(resp.data)
			sessionStorage.setItem('settings_digitalmenu', JSON.stringify(resp.data.setting))
			setTheme(createMuiTheme({
				palette: {
				  primary: {
					main: resp.data.setting.main_color,
					contrastText: resp.data.setting.text_color
				  },
				  secondary: {
					main: resp.data.setting.secondary_color
				  }
				}
			}))
			setisLoading(false)
		})
	}, [name])

	
	return !isLoading? (
		<ThemeProvider theme={theme}>
			<div className="restaurant">
				<AppBar>
					<Toolbar className="header">
						<div style={{ color: theme.palette.primary.contrastText }} className="header__title">
							{name.replace('-', ' ') }
						</div>
						<div className="header__icon">
							{
								Restaurant.image !== '' ?<img alt="restaurant" src={Restaurant.image}  />:null
							}
						</div>
					</Toolbar>
				</AppBar>
				<div className="restaurant__main">
					<div className="restaurant__carrousel">
						{
							Restaurant.menus.length > 0?
							<Carrousel name="promos" size="medium" title="Promociones" sliders={Restaurant.menus} /> :
							<Skeleton className="restaurant__skeleton" variant="rect" width={'100%'} height={150} />
						}
					</div>
					<div className="restaurant__carrousel">
						{
							Restaurant.menus.length > 0?
							<Carrousel name="menus" size="large" title="Menús Disponibles" sliders={Restaurant.menus} /> :
							<Skeleton className="restaurant__skeleton" variant="rect" width={'100%'} height={275} />
						}
					</div>
				</div>
				<OrderButton top={90} left={56} setisOpen={() => setisOpen(true)} />
				<Dialog
					open={isOpen}
					onClose={() => setisOpen(false)}
				>
					<span onClick={() => setisOpen(false)} className="menu__close">&times;</span>
					<OrderDialog />
				</Dialog>
				{/* <Dialog
					open={isPromo}
					onClose={() => {
						setCookie('show_promos', false, { maxAge: 60 })
						setisPromo(false)
					}}
				>
					<span onClick={() => {
						setCookie('show_promos', false, { maxAge: 60 })
						setisPromo(false)
					}} className="menu__close">&times;</span>
					<PromoDialog />
				</Dialog> */}
			</div>
		</ThemeProvider>
	):null
}
